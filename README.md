To run the test app:

1. Install the dependencies:
```bash
pip install -r requirements.txt
```

2. Start the app: 
```bash
python app.py
```

The app is listening on http://localhost:5000/
